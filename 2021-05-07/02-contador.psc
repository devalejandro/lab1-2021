Algoritmo contador
	// Cargar un vector/arreglo de 100 elementos con numeros aleatorios entre 1 y 10
	// Mostrar cuantas veces aparecen los numeros entre 1 y 10
	Dimension nums[100]
	Dimension cont[10]
	
	// Cargar vector
	para i=1 hasta 100
		nums[i] = aleatorio(1,10)
	FinPara
	
	// contar
	para i=1 hasta 100
		cont[nums[i]] = cont[nums[i]] + 1
	FinPara
	
	// Mostrar la cuenta
	para i=1 hasta 10
		escribir 'El numero ' i ' se repite ' cont[i] ' veces'
	FinPara
FinAlgoritmo
