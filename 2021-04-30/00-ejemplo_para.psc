Algoritmo ejemplo_para
	// Para comun
	Para i=0 Hasta 10 Hacer
		escribir i
	Fin Para
	
	// Para con paso
	Para i=0 Hasta 10 con paso 3 Hacer
		escribir i
	Fin Para
	
	// Para descendiente
	Para i=10 Hasta 0 con paso -1 Hacer
		escribir i
	Fin Para
	
	// Para con decimales
	Para i=0 Hasta 10 con paso 0.5 Hacer
		escribir i
	Fin Para
FinAlgoritmo
