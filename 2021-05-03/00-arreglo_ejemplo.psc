Algoritmo arreglo_ejemplo
	// Crear arreglo
	dimension arreglo[5]
	
	// cargar arreglo
	leer arreglo[1]
	leer arreglo[2]
	leer arreglo[3]
	leer arreglo[4]
	leer arreglo[5]

	// mostrar arreglo
	escribir arreglo[1]
	escribir arreglo[2]
	escribir arreglo[3]
	escribir arreglo[4]
	escribir arreglo[5]
	
FinAlgoritmo
